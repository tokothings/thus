// ==UserScript==
// @name 				 tokotna hp updater script (t.h.u.s.)
// @match				 *://tokotna.com/imports/hp/edit/index.php*
// @grant        none
// @require      https://tokotna.com/js/external/jquery/jquery.js
// @run-at       document-end
// @encoding     utf-8
// @version      1.0
// @description  script to add a variety of functionality to the tokotna.com hp tracking tool. Tokotna Version: 1.3.2.
// @author       majfisch @deviantArt and tokotna.com
// ==/UserScript==

// snippet from https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
function escapeRegExp(string) {
   return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  // $& means the whole matched string
}

function thusUpdateTotal(cols) {
  let newValues = $(cols[1]).text().split("+");
  newValues.unshift(0);
  let newTotal = 0;
  newTotal = newValues.reduce(function(summed, currentVal){
    currentVal = parseInt(currentVal.slice(0,1));
    if (!isNaN(currentVal)) {
      return parseInt(summed) + currentVal;
    } else {
      return summed;
                              }});
	$(cols[2]).text(newTotal);
  return newTotal;
}

function thusReplaceHP(oldString, newString) {
  if (oldString == "") {
      $("#msg").html("nothing to replace");
      return;
  }
  let totalRemoved = 0;
  const oldRegEx = new RegExp(oldString, 'g');
  console.log(oldRegEx);
  // look for entries in that contain searchstring and replace
  $("#hpTable tbody tr").each(function() {
    let row = $(this);
    let cols = row.children("td");
    const oldBreakdown = $(cols[1]).text();
    const newBreakdown = oldBreakdown.replace(oldRegEx, newString);
    $(cols[1]).html(newBreakdown);
  	// update total
    totalRemoved += $(cols[2]).text()- thusUpdateTotal(cols);
  });
  // show a little msg
  let action;
  if (totalRemoved >= 0) {
    action = "removed";
  } else {
    action = "added";
  }
  $("#msg").html("<b>" + Math.abs(totalRemoved) + " HP " + action + ". " +
                 "Remember to double check if everything is correct!</b>");
}

function thusAddHP(hpText, userInputId, notIncluding) {
  let totalAdded = 0;
  // get username from Input field
  const daUser = $("#" + userInputId).val();
  const userRegEx = new RegExp("\\/"+daUser+"\\/",'i');
  const hpTextRegEx = new RegExp(hpText,'i');
  const notIncludingRegEx = new RegExp(notIncluding,'i');
  //remove escapes from text
  hpText = hpText.replaceAll("\\", "");
  // go through table to find matching lines and add
  $("#hpTable tbody tr").each(function() {
    let row = $(this);
    let cols = row.children("td");
    // check if user matches
    if (daUser === "" || userRegEx.test($(cols[0]).text())) {
      // add at end if text is not there already
      // AND notIncluding is also not there
      if (!hpTextRegEx.test($(cols[1]).text())) {
        if (notIncluding === undefined || !notIncludingRegEx.test($(cols[1]).text())) {
         	cols[1].append(" " + hpText);
        	// recalculate HP total and write into table, add it to total added
        	totalAdded += - parseInt($(cols[2]).text()) + thusUpdateTotal(cols);
        }
      }
    }
  });
  // show msg
  $("#msg").html("<b>" + totalAdded + " HP added. "+
                 "Remember to double check if everything is correct!</b>");
}

function thusReplaceSpecific(partialOldString, newString, userInputIdOld, userInputIdNew) {
  const oldString = partialOldString.replace("INPUTHERE", escapeRegExp($("#" + userInputIdOld).val()));
  newString = newString.replace("INPUTHERE", $("#" + userInputIdNew).val());
  thusReplaceHP(oldString, newString);
}

function thusReplaceTwoPart(oldFront, oldMiddle, oldEnd, newFront, newEnd) {
    // replace after middle -
    // this is first so the msg about the amount of HP added isn't set to 0 at the end
  const regExStringEnd = "\(\(\?\<\=\(" + oldMiddle + "\)\)" + oldEnd + "\)";
  thusReplaceHP(regExStringEnd, newEnd);

  // replace before middle
  const regExStringFront = "\(" + oldFront + "\(\?\=\(" + oldMiddle + "\)\)\)";
  console.log(regExStringFront);
  thusReplaceHP(regExStringFront, newFront);

}

function thusBecome(faction) {
  let otherFaction = "PL";
  if (faction === "PL") {
    otherFaction = "LK";
  }
  // these wild looking strings are RegExp, \\ is for escaping +, (, )
  thusReplaceHP("(\\+2\\(breed\\)|\\+3\\("+otherFaction+" breed\\))", "+3("+faction+" breed)");
  thusReplaceHP("(\\+2\\(AR\\)|\\+3\\("+otherFaction+" AR\\))", "+3("+faction+" AR)");
}

function thusAddMany() {
  // retrieve links, HP breakdown and total
  let dALinks = $("#linkname").val().split(/(?=https\:)/g);
  let hpBreakdown = $("#breakdown").val();
  let hpTotal = $("#totalhp").val();
  // add to table by filling out form and clicking regular add button X times
  dALinks.forEach((link, index) => {
    $("#linkname").val(link);
  	$("#breakdown").val(hpBreakdown);
   	$("#totalhp").val(hpTotal);
    $("#updateButton").click();
  });
}

function thusAddButton(buttonText, buttonFunction, buttonFunctionParams, divElem, desc) {
  const newButton = $('<button type="button" class="button">').html(buttonText);
  newButton.click(function() {
    buttonFunction(...buttonFunctionParams);
  });
  newButton.attr("title", desc);
  divElem.append(newButton);
}

function thusAddButtons() {
  // create layout elements
  const thusContainer = $("<details>");
  thusContainer.append($("<summary>").html("more tokotna hp updater script (t.h.u.s.)"));
  const addManyDiv = $("<div>");
  const fBonusDiv = $("<div>");
  const ownerChangeDiv = $("<div>");
  const miscDiv = $("<div>");
  const inputDiv= $("<div>");
  thusContainer.append(fBonusDiv, ownerChangeDiv, miscDiv, inputDiv);

  // add thus add button before add button
  $("#clearButton").parent().prepend(addManyDiv);

  // add buttons after clear button
  const buttonsDiv = $("<div>");
  $("#clearButton").parent().parent().after(buttonsDiv);
  buttonsDiv.append(thusContainer);

  thusAddButton("t.h.u.s. Add", thusAddMany,[], addManyDiv,
               "Add one or many with same HP breakdown; enter links in link input field without separation")
  // buttons for faction change
  thusAddButton("Remove faction AR/breed Bonus", thusReplaceHP,
                ['(\\+3\\(PL |\\+3\\(LK )', '+2('], fBonusDiv);
  thusAddButton("Add PL Bonus", thusBecome, ["PL"], fBonusDiv);
  thusAddButton("Add LK Bonus", thusBecome, ["LK"], fBonusDiv);

  // add buttons for owner and/or tribe change
  // remove non-com for all
  thusAddButton("Remove non-com", thusReplaceHP, [" \\+4\\(non-com\\)", ""], ownerChangeDiv,
               "removes non-com bonus from all pieces");
  // remove all collab
  thusAddButton("Remove collab", thusReplaceHP, [" \\+2\\(collab\\)", ""], ownerChangeDiv,
               "removes collab bonus from all pieces");
  // add non-com for all pieces of one dA user
  thusAddButton("Add non-com for...", thusAddHP,
                ["\\+4\\(non-com\\)", "userOrIdInput", "\\+2\\(collab\\)"], ownerChangeDiv,
               "adds non-com (at end) for all pieces by artist specified in input field; " +
               "all images if no artist is specified; " +
               "does not add to pieces that already have non-com or collab bonus");
  // add collab for all pieces of one dA user
  thusAddButton("Add collab for...", thusAddHP,
                ["\\+2\\(collab\\)", "userOrIdInput", "\\+4\\(non-com\\)"], ownerChangeDiv,
               "adds collab (at end) for all pieces by artist specified in input field; " +
               "all images if no artist is specified; " +
               "does not add to pieces that already have non-com or collab bonus");
  // remove all tribemates
  thusAddButton("Remove all tribemates", thusReplaceHP,
                [" \\+2\\(\\[.{3,6}\\] is a tribemate\\)", ""], ownerChangeDiv,
               "removes any tribemate bonus from all pieces");
  // remove specific tribemate
  thusAddButton("Remove tribemate with ID ...", thusReplaceSpecific,
                [" \\+2\\(\\[" + "INPUTHERE" + "\\] is a tribemate\\)", "", "userOrIdInput"], ownerChangeDiv,
               "removes tribemate bonus for Tokota with ID specified in input field from all pieces")
  // upgrade tribe achievement "birds of a feather" to "flock together"
  thusAddButton("Upgrade birds to flock", thusReplaceTwoPart,
                [" \\+2\\(", "\\[.\{3,6\}\\] is a tribemate", "\\)" , " \+3\(", " HC\)"], ownerChangeDiv,
               "upgrades all tribemate bonuses from +2 bonus to +3 bonus and adds HC");
  // upgrade superstar I to II
  thusAddButton("Upgrade to Superstar II ...", thusReplaceSpecific,
                [" \\+2\\(\\[" + "INPUTHERE" + "\\] is superstar\\)",
                 " +3([" + "INPUTHERE" + "] is superstar II)",
                 "userOrIdInput", "userOrIdInput"], miscDiv,
               "Upgrades superstar trait bonus of Tokota with ID specified in input field for all pieces")
  // input to specify which exactly
  const userOrIdInput = $("<input id='userOrIdInput' placeholder='dAUser / Tokota ID'>");
  inputDiv.append(userOrIdInput);
  // input for replacements
  thusAddButton("... custom replace by...", thusReplaceSpecific,
               ["INPUTHERE", "INPUTHERE", "userOrIdInput", "replacementInput"], inputDiv,
               "Replaces value in box by value in box after the button in all pieces");
  const replacementInput = $("<input id='replacementInput' placeholder='replacement (for custom replacement only)'>");
  inputDiv.append(replacementInput);
}

if ($("div:contains('ersion: 1.3.2')").length !== 0) {
  thusAddButtons();
} else {
  $("#clearButton").parent().parent().after("<div>Your version of t.h.u.s. is not made for this version of tokotna.com; Look for an update if you want to use it, please.</div>");
}
