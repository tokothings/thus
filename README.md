# Tokotna HP updater script
## Description
This is a Userscript for use with tokotna.com. It lets the user update their Tokotas' HP trackers in a variety of ways (see Usage for more details)
## Installation
To use the script you will need a userscript manager as a browser add-on. Which ones are available depends on the browser you are using. I use firefox with the firemonkey add-on.

[Here's a very brief article on how to use Firemonkey](https://www.ilovefreesoftware.com/11/windows/internet/plugins/how-to-use-userscripts-userstyles-firemonkey-firefox.html)

Instead of pasting the script you can also download the file and import it using the import button in the list of scripts.
 
Once you have the script installed it will modify your HP tracker editing pages everytime you open them by adding a few buttons which are explained in the Usage section below.

## Usage

This script can't do anything you couldn't do manually. However, it is always a good idea to double check if it didn't mess up your tracker by accident - because it can! The author of the script will not be held responsible if you lose the information on your tracker by saving after having messed up the tracker.

If you ever click a button by mistake, you can click the cancel button on tokotna. This will undo all changes made since the last save (which is a native feature of the site).

As before, you need to click "Save" in order to keep your changes.

**Please note**: many of the buttons only work if your tracker entries use the exact format the HP Calculator tool provides! If they have a different format, you can use the "custom replace by" button to convert them in many cases. 

On to what the buttons do:
### t.h.u.s. add
This is the only button that is always visible. When you click it, it will add one *or multiple* pieces to your tracker. The pieces will all have the same HP breakdown

To add multiple pieces: 
1. paste the URLs (link adresses) of the pieces into the "Link" input field without any spaces or other characters in between. 
> Example:
> To add https://linkA and https://linkB to the tracker, enter this into the Link field:
> https://linkAhttps://linkB
2. Add the HP breakdown. 
3. Click the "t.h.u.s. add" button.

**How it works**: The script divides what you put into the Link field into multiple links. It cuts them off at the "https:" part. It then fills out the form with each link and clicks the original "Add" button - much faster than you can even see.

###  Remove faction AR Bonus, Add PL or LK Bonus
These buttons add or remove the activity and breeding image bonus for all pieces that are already in the tracker.

### Remove non-com or collab
These buttons remove all non-com or collab bonuses from all pieces in the tracker.

### Add non-com or collab for ...
These buttons add the non-com or collab bonus for all pieces that were uploaded to dA by the user specified in the first input field.

### Remove all tribemates
Removes the tribemate bonus from all pieces.

### Remove tribemate with ID ...
Removes the tribemate bonus with the ID specified in the first input field.
This is intended for when your tribemate has traded away the tokota or left the tribe.

### Upgrade birds to flock
Upgrades all +2 tribemate bonuses to +3 and adds HC at the end to match the HP calculator format of flock together.

### Upgrade to Superstar II
Upgrades all +2 Superstar bonuses to +3 Superstar III bonuses for the tokota with the ID specified in the first input field.

### ... custom replace by ...
Replaces all instances of what is entered in the first input field with what is entered in the second.

## Support
If you run into issues you may contact me on Discord or post on [this deviantArt journal](https://www.deviantart.com/majfisch/journal/tokotna-hp-updater-script-t-h-u-s-945009092).

## Roadmap
There are currently no plans of expanding on this script. Feel free to offer suggestions however.

## License
You may do anything you want with this script, including modifying and publishing the code. Please give me credit if you do so. Use at your own risk.

The script is completely free to use. If you like it and would like to compensate me for the hours put into writing it and preparing it for sharing, you may donate in-gamestuff to me on Tokotna (username is majfisch). C:
